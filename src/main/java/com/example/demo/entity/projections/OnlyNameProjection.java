package com.example.demo.entity.projections;

import com.example.demo.entity.House;
import com.example.demo.entity.Tenant;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "NameAndHouse", types = {Tenant.class})
public interface OnlyNameProjection {
    String getName();

    House getHouse();
}
