package com.example.demo.entity.projections;

import com.example.demo.entity.Tenant;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "mobileAndName", types = {Tenant.class})
public interface TenantProjection {
    String getName();

    String getMobile();
}
