package com.example.demo.configuration;

import com.example.demo.entity.Tenant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.rest.core.annotation.HandleAfterDelete;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RepositoryEventHandler
public class TenantEventHandler {
    @HandleBeforeDelete
    protected void onBeforeDelete(Tenant entity) {
        log.info("现在要开始删除操作了，删除对象:{}", entity);
    }

    @HandleAfterDelete
    protected void onAfterDelete(Tenant entity) {
        log.info("删除对象完成，删除对象:{}", entity);
    }

}
