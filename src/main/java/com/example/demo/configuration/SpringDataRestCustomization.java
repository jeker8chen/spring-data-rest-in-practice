package com.example.demo.configuration;

import com.example.demo.configuration.validator.SaveTenantValidator;
import com.example.demo.entity.Tenant;
import com.example.demo.entity.projections.TenantProjection;
import com.example.demo.repository.TenantRepository;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.core.mapping.ExposureConfiguration;
import org.springframework.data.rest.core.mapping.RepositoryDetectionStrategy;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
public class SpringDataRestCustomization implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//        config.withEntityLookup()
//                .forRepository(TenantRepository.class)
//                .withIdMapping(Tenant::getId)
//                .withLookup(TenantRepository::findById);

        config.getProjectionConfiguration().addProjection(TenantProjection.class);
        config.setRepositoryDetectionStrategy(RepositoryDetectionStrategy.RepositoryDetectionStrategies.DEFAULT);

        ExposureConfiguration exposureConfiguration = config.getExposureConfiguration();
        exposureConfiguration.forDomainType(Tenant.class);
//                .disablePutForCreation()
//                .withItemExposure((metadata, httpMethods) -> httpMethods.disable(HttpMethod.DELETE));
    }

    @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate", new SaveTenantValidator());
    }
}
