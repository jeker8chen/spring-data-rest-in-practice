package com.example.demo.repository;

import com.example.demo.entity.House;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface HouseRepository extends CrudRepository<House, Long> {
}
